#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <MQUnifiedsensor.h>
#include <string.h>
#include <string>
#include <iostream>
#include "DHT.h"

#define SCREEN_WIDTH 128 // OLED width,  in pixels
#define SCREEN_HEIGHT 64 // OLED height, in pixels
#define DHT_SENSOR_PIN  12
#define DHT_SENSOR_TYPE DHT11

#define placa "ESP-32"
#define Voltage_Resolution 3.3
#define pin 34
#define type "MQ-135"
#define ADC_Bit_Resolution 12
#define RatioMQ135CleanAir 3.6

Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
double CO2 = (0); 
MQUnifiedsensor MQ135(placa, Voltage_Resolution, ADC_Bit_Resolution, pin, type);
DHT dht(DHT_SENSOR_PIN, DHT_SENSOR_TYPE);

// =============== Function prototype ================
void initScreen(void);
void writeScreen(String msg);
void writeCO2Screen(double value);
void initMQ135(void);

// ======================= Main ===================

void setup() {
  Serial.begin(115200);
  delay(2000);
  initScreen();
  initMQ135();
  dht.begin();
}

// ======================= End of main  ===================

void loop() {
  MQ135.update();
  CO2 = MQ135.readSensor();
  writeCO2Screen(CO2);
  

  float humi  = dht.readHumidity();
  float tempC = dht.readTemperature();
  float tempF = dht.readTemperature(true);
  
  if ( isnan(tempC) || isnan(tempF) || isnan(humi)) {
    Serial.println("Failed to read from DHT sensor!");
  } else {
    Serial.print("Humidity: ");
    Serial.print(humi);
    Serial.print("%");

    Serial.print("  |  ");

    Serial.print("Temperature: ");
    Serial.print(tempC);
    Serial.print("°C  ~  ");
    Serial.print(tempF);
    Serial.println("°F");
  }

  delay(1000);
}

void initScreen() {
  if (!oled.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("failed to start SSD1306 OLED"));
    while (1);
  }
}

void initMQ135() {
  MQ135.setRegressionMethod(1);
  MQ135.setA(110.47);
  MQ135.setB(-2.862);

  MQ135.init();
  Serial.println("Calibration du MQ135 en cours ...");

  float calcR0 = 0;   
  for(int i = 1; i<=10; i ++)   {     
      MQ135.update(); // Update data, the arduino will be read the voltage on the analog pin
      calcR0 += MQ135.calibrate(RatioMQ135CleanAir);
      Serial.print(".");
  }

  MQ135.setR0(calcR0/10);
  Serial.println("  done!.");  
  if (isinf(calcR0)) { 
    Serial.println("Warning: Conection issue founded, R0 is infite (Open circuit detected) please check your wiring and supply"); 
    while(1);
  }
  if (calcR0 == 0) {
    Serial.println("Warning: Conection issue founded, R0 is zero (Analog pin with short circuit to ground) please check your wiring and supply");
    while(1);
  }
  MQ135.serialDebug(false); 
}

void writeCO2Screen(double value) {
  oled.clearDisplay(); // clear display

  oled.setTextSize(1);         // set text size
  oled.setTextColor(WHITE);    // set text color
  oled.setCursor(0, 10);       // set position to display
  oled.print("CO2 : ");
  oled.println(value); // set text
  oled.display(); 
}

void writeScreen(String msg) {
  oled.clearDisplay(); // clear display

  oled.setTextSize(1);         // set text size
  oled.setTextColor(WHITE);    // set text color
  oled.setCursor(0, 10);       // set position to display
  oled.println(msg); // set text
  oled.display(); 
}